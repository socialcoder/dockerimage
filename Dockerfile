FROM php:5.6-apache

MAINTAINER Stumbler <bucher@navigate.de>

# Fix docker-php-ext-install script error
RUN sed -i 's/docker-php-\(ext-$ext.ini\)/\1/' /usr/local/bin/docker-php-ext-install

# Install other needed extensions
RUN apt-get update && apt-get install -y apt-utils libxml2-dev php-soap libfreetype6 git-core mysql-client ghostscript imagemagick libjpeg62-turbo libmcrypt4 libpng12-0 sendmail --no-install-recommends && rm -rf /var/lib/apt/lists/*
RUN buildDeps=" \
		libfreetype6-dev \
		libjpeg-dev \
		libldap2-dev \
		libmcrypt-dev \
		libpng12-dev \
		zlib1g-dev \
		libmagickwand-dev \
	"; \
	set -x \
	&& apt-get update && apt-get install -y $buildDeps --no-install-recommends && rm -rf /var/lib/apt/lists/* \
	&& docker-php-ext-configure gd --enable-gd-native-ttf --with-jpeg-dir=/usr/lib/x86_64-linux-gnu --with-png-dir=/usr/lib/x86_64-linux-gnu --with-freetype-dir=/usr/lib/x86_64-linux-gnu \
	&& docker-php-ext-install gd \
	&& docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu \
	&& docker-php-ext-install ldap \
	&& docker-php-ext-install mbstring \
	&& docker-php-ext-install mcrypt \
	&& docker-php-ext-install mysqli \
	&& docker-php-ext-install opcache \
	&& docker-php-ext-install soap \
	&& docker-php-ext-install pdo_mysql \
	&& docker-php-ext-install zip \
	&& docker-php-ext-install sockets \
	&& pecl install imagick \
	&& docker-php-ext-enable imagick \
	&& apt-get purge -y --auto-remove $buildDeps \
	&& mkdir /usr/src/php \
	&& mkdir /usr/src/php/ext \
	&& cd /usr/src/php \

	&& curl -L https://pecl.php.net/get/xdebug-2.3.3.tgz >> /usr/src/php/ext/xdebug.tgz \
	&& tar -xf /usr/src/php/ext/xdebug.tgz -C /usr/src/php/ext/ \
	&& rm /usr/src/php/ext/xdebug.tgz \
	&& docker-php-ext-install xdebug-2.3.3

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.enable=1'; \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini


# PECL extensions
RUN pecl install APCu-4.0.10 redis \
	&& docker-php-ext-enable apcu redis soap
# Install Composer for Laravel
RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer

# Install Docker To Do Magical Wonderful Things
RUN curl -sSL https://get.docker.com/ | sh

# Setup timezone to Etc/UTC
RUN cat /usr/src/php/php.ini-production | sed 's/^;\(date.timezone.*\)/\1 \"Etc\/UTC\"/' > /usr/local/etc/php/php.ini

#setup php error loggin
#RUN echo "log_errors = 1" >> /usr/local/etc/php/php.ini
RUN echo "error_log = /var/log/apache2/error.log" >> /usr/local/etc/php/php.ini

#setup xdebug
RUN echo "" >> /usr/local/etc/php/php.ini
RUN echo "xdebug.remote_enable = 1" >> /usr/local/etc/php/php.ini
RUN echo "xdebug.idekey = \"PHPSTORM\"" >> /usr/local/etc/php/php.ini
RUN echo "xdebug.remote_autostart = 1" >> /usr/local/etc/php/php.ini
#RUN echo "xdebug.remote_connect_back = 1" >> /usr/local/etc/php/php.ini
RUN echo "xdebug.remote_port = 9000" >> /usr/local/etc/php/php.ini
RUN echo "xdebug.remote_handler=dbgp" >> /usr/local/etc/php/php.ini
RUN echo "xdebug.remote_host=10.254.254.254" >> /usr/local/etc/php/php.ini
RUN echo "xdebug.remote_log=\"/tmp/xdebug.log\"" >> /usr/local/etc/php/php.ini

# Disable cgi.fix_pathinfo in php.ini
RUN sed -i 's/;\(cgi\.fix_pathinfo=\)1/\10/' /usr/local/etc/php/php.ini

RUN usermod -u 1000 www-data && a2enmod rewrite

#CMD "usermod -a -G docker www-data"



WORKDIR /var/www/html